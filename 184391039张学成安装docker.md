## docker引擎的安装

### 修改主机名

hostnamectl set-hostname master

刷新

bash

### 配置网卡（访问外网）

### 基础环境配置

#### 将Docker.tar.gz上传至虚拟机中

解压Docker.tar.gz

tar -zxvf  Docker.tar.gz

#### 配置本地yum源

vi /etc/yum.repos.d/local.repo



[kubernetes]

name=kubernetes

baseurl=file:///root/Docker

gpgcheck=0

enabled=1

#### 升级系统内核

不要移除网络源（/etc/yum.repos.d/）移除了没有网络源无法升级系统内核

![image-20200916202150295](C:\Users\123\AppData\Roaming\Typora\typora-user-images\image-20200916202150295.png)

yum upgrade -y

uname -r

#### 关闭防火墙&清除防火墙规则&关闭selinux

systemctl stop firewalld 

systemctl disable firewalld 



iptables -F

iptables -X

iptables -Z

 

getenforce (查看selinux状态)

setenforce 0(临时关闭)

#### 开启路由转发

[root@master ~]# cat >> /etc/sysctl.conf << EOF

net.ipv4.ip_forward=1

net.bridge.bridge-nf-call-ip6tables = 1

net.bridge.bridge-nf-call-iptables = 1

EOF

### docker引擎安装

#### 安装依赖包

yum-utils提供了yum-config-manager的依赖包，device-mapper-persistent-data和lvm2are需要devicemapper存储驱动。

yum install -y yum-utils device-mapper-persistent-data

#### 安装docker-ce

yum install -y docker-ce-18.09.6 docker-ce-cli-18.09.6 containerd.io

#### 启动docker

systemctl daemon-reload

systemctl restart docker

systemctl enable docker

查看docker 的系统信息

docker info

出现如下：

  Containers: 0

 Running: 0

 Paused: 0

 Stopped: 0

Images: 0

Server Version: 18.09.6

Storage Driver: devicemapper
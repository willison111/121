### ftp

挂载光盘

mount /dev/cdrom  /opt/centos       创建一个目录opt/centos

备份系统自带的yum源

mv /etc/yum.repos.d/*  /etc/mak

新建一个yum.repo

vi /etc/yum.repos.d/local.repo

[centos]

name=centos

baseurl=file:///opt/centos

gpgcheck=0

enabled=1

测试yum源

yum clean all

yum repolist

安装vsftpd服务

yum -y install vsftpd

修改配置文件

vi /etc/vsftpd/vsftpd.conf

添加  anon_root=/opt

重启服务  systemctl start vsftpd

使用浏览器登陆验证

ftp://192.168.96.129

客户端使用ftp

安装ftp

yum -y install vsftpd

备份系统自带的yum源

mv /etc/yum.repos.d/*  /etc/mak

新建一个yum.repo

vi /etc/yum.repos.d/local.repo

[centos]

name=centos

baseurl=ftp://192.168.96.129/centos

gpgcheck=0

enabled=1

测试yum源

yum clean all

yum repolist

### nfs

是cs架构

#### 服务端

安装（nfs-utils和rpcbind）

使用yum工具安装会一并安装

yum install -y  nfs-utils

修改配置文件

vi /etc/exports   

/home/nfstestdir 192.168.96.0/24(rw,sync,all_squash,anonuid=100,anongid=100)

第一部分 本地要共享出去的目录

第二部分 允许访问的主机（可以是IP，也可以是网段）

第三部分 权限选项  

​                rw:读写 

​                sync:同步模式，表示内存中的数据实时写入磁盘

no_root_squash:root用户会对共享的目录拥有至高的权限控制   安全性降低

root_sqash:与no_root_squash 选项相对应，对共享目录的权限不高，只有普通用户的权限

all_squash:表示不管使用NFS的用户是谁，其身份都会被限定为一个指定的普通用户身份

anonuid/anongid:指定uid和gid

exportfs -r    配置文件生效

创建共享目录

mkdir /home/nfstestdir

启动nfs服务    在启动nfs服务之前时需要先启动rpcbind服务

systemctl start rpcbind

systemctl start nfs

#### 客户端

##### 安装 nfs

yum install -y  nfs-utils

查看共享了那些目录

showmount -e 192.168.96.129

-e 列出服务端以挂载的目录

在客户端挂载nfs

mount -t nfs 192.168.96.129:/home/nfstestdir /mnt/

查看已经挂载的

df -h

查看到增加了一个/mnt 分区，他就是nfs共享的目录了

进入到 /mnt 目录下测试文件

cd /mnt

创建文件

touch  123.txt

touch:无法创建“123.txt”：权限不够

这是因为在服务端创建的/home/nfstestdir目录权限不够

进入到服务端修改权限

chmod 777 /home/nfstestdir

再回到客户端上创建文件夹测试

cd /mnt

touch 123.txt

可以看到创建的新文件夹

### samba

安装samba 服务

yum install -y samba

编辑samba配置文件

vi /etc/samba/sam.conf

load printers = no

​    cups options = raw

 

;    printcap name = /dev/null

​     #obtain a list of printers automatically on UNIX System V systems:

;    printcap name = lpstat

;    printing = bsd

​    disable spoolss = yes

创建共享目录

mkdir /opt/share

修改共享目录权限

chmod 777 /opt/share

启动samba守护进程  

systemctlstart smb

systemctl start nmb

创建samba用户、密码

sambapasswd -a root

安装net工具

yum install -y net-tools

查看端口

netstat -ntpl |grep 445

windows验证

在运行框输入 \\\192.168.96.129

![image-20200831194227615](C:\Users\123\AppData\Roaming\Typora\typora-user-images\image-20200831194227615.png)

输入用户名及密码

即可看到共享的目录了

#### 排错

子网掩码不同

同步不过来：配置文件出错

防火墙没有关闭

selinux 没有关闭

查看配置文件